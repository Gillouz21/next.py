#unit1
#1.1.2
import functools


def join_letters(char, char2):
    return char + (char2*2)

def double_letter(my_str):
    return functools.reduce(join_letters, my_str, "")

print(double_letter("python"))
print(double_letter("we are the champions!"))

#1.1.3
def helper(num):
    return num % 4 == 0
def four_dividers(number):
    return list(filter(helper, range(1,number)))

print(four_dividers(9))
print(four_dividers(3))

#1.1.4
import functools


def sum_2_digits(acc, digit):
    return acc + int(digit)

def sum_of_digits(number):
    return functools.reduce(sum_2_digits, str(number), 0)

print(sum_of_digits(104))

#1.3.1
def intersection(list_1, list_2):
    return list(filter(lambda x: x in set(list_2), set(list_1)))

print(intersection([1, 2, 3, 4], [8, 3, 9]))
print(intersection([5, 5, 6, 6, 7, 7], [1, 5, 9, 5, 6]))

#1.3.2
def is_prime(number):
    return all([number % x != 0 for x in range(2, number) if x != number])

print(is_prime(42))
print(is_prime(43))

#1.3.3
def is_funny(string):
    return all([char == 'h' or char == 'a' for char in string])

print(is_funny("hahahahahaha"))

#1.3.4
password = "sljmai ugrf rfc ambc: lglc dmsp mlc rum"
print(''.join([chr((ord(x) - ord('a') + 2) % 26 + ord('a')) if x.isalpha() else x for x in password]))

#1.5.1
fileos = open("C:\\Users\\User\PycharmProjects\\next.py\\file1.5.txt", 'r')
print(max(fileos, key=lambda x: len(x)))

#1.5.2
fileos = open("C:\\Users\\User\PycharmProjects\\next.py\\file1.5.txt", 'r')
print(sum([len(x.rstrip('\n')) for x in fileos ]))

#1.5.3
fileos = open("C:\\Users\\User\PycharmProjects\\next.py\\file1.5.txt", 'r')
min_length = len(min(fileos.read().split(), key=len))
fileos = open("C:\\Users\\User\PycharmProjects\\next.py\\file1.5.txt", 'r')
print(''.join([x for x in fileos if len(x.rstrip('\n')) == min_length]))

#1.5.4
f_len = open("C:\\Users\\User\PycharmProjects\\next.py\\name_length.txt", 'w')
f_names = open("C:\\Users\\User\PycharmProjects\\next.py\\file1.5.txt", 'r')
f_len.writelines([str(len(x.strip('\n'))) + '\n' for x in f_names])

#1.5.5
num = input("Enter name length")
f = open("C:\\Users\\User\PycharmProjects\\next.py\\file1.5.txt", 'r')
print(''.join([x for x in f if len(x.strip('\n')) == int(num)]))

#unit2
#2.2.2
class Monkey:

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def birthday(self):
        self.age += 1

    def get_age(self):
        return self.age



james = Monkey("James", 13)
jessica = Monkey("Jessica", 18)

james.birthday()
print(james.age)
print(jessica.age)

#2.3.3
class Monkey:
    count = 0

    def __init__(self, age, name="Octavio"):
        self._name = name
        self._age = age
        Monkey.count += 1

    def birthday(self):
        self._age += 1

    def get_age(self):
        return self._age

    def set_name(self, new_name):
        self._name = new_name

    def get_name(self):
       return self._name

james = Monkey(13, "James")
octavio = Monkey(18)

print(james.get_name())
print(octavio.get_name())
octavio.set_name("yuvi")
print(octavio.get_name())
print(octavio.count)

#2.3.4
class Pixel:

    def __init__(self, x=0, y=0, red=0, green=0, blue=0):
        self._x = x
        self._y = y
        self._red = red
        self._green = green
        self._blue = blue

    def set_coords(self, x, y):
        self._x = x
        self._y = y

    def set_grayscale(self):
        val = (self._red + self._green + self._blue) // 3
        self._red = val
        self._green = val
        self._blue = val

    def print_pixel_info(self):
        info = "X: {}, Y: {}, Color: ({}, {}, {})".format(self._x, self._y, self._red, self._green, self._blue)
        adding = ""
        if(self._red > 50 and self._green == 0 and self._blue == 0):
            adding = " Red"
        if (self._red == 0 and self._green > 50 and self._blue == 0):
            adding = " Green"
        if (self._red == 0 and self._green == 0 and self._blue > 50):
            adding = " Blue"

        info += adding
        print(info)


pix = Pixel(5, 6, 250)
pix.print_pixel_info()
pix.set_grayscale()
pix.print_pixel_info()

#2.4.2
class BigThing:
    def __init__(self, val):
        self.val = val

    def size(self):
        if isinstance(self.val, int):
            return self.val
        else:
            return len(self.val)

class BigCat(BigThing):

    def __init__(self, name,  size):
        BigThing.__init__(self, name)
        self.size_value = size

    def size(self):
        if self.size_value > 20:
            return "Very Fat"
        elif self.size_value > 15:
            return "Fat"
        else:
            return "OK"

my_thing = BigThing("balloon")
print(my_thing.size())
cutie = BigCat("mitzy", 22)
print(cutie.size())

#2.5
class Animal:
    zoo_name = "Hayaton"

    def __init__(self, name, hunger=0):
        self._name = name
        self._hunger = hunger

    def get_name(self):
        return self._name

    def is_hungry(self):
        return self._hunger > 0

    def feed(self):
        self._hunger -= 1


class Dog(Animal):

    def __init__(self, name, hunger=0):
        Animal.__init__(self, name, hunger)

    def talk(self):
        print("woof woof")

    def fetch_stick(self):
        print("There you go, sir!")
class Cat(Animal):

    def __init__(self, name, hunger=0):
        Animal.__init__(self, name, hunger)

    def talk(self):
        print("meow")

    def chase_laser(self):
        print("Meeeeow")

class Skunk(Animal):

    def __init__(self, name, hunger=0, stink_count=6):
        Animal.__init__(self, name, hunger)
        _stink_count = stink_count

    def talk(self):
        print("tssssss")

    def stink(self):
        print("Dear lord!")
class Unicorn(Animal):

    def __init__(self, name, hunger=0):
        Animal.__init__(self, name, hunger)

    def talk(self):
        print("Good day, darling")

    def sing(self):
        print("I’m not your toy...")
class Dragon(Animal):

    def __init__(self, name, hunger=0, color="Green"):
        Animal.__init__(self, name, hunger)
        _color = color

    def talk(self):
        print("Raaaaaaawr")

    def breath_fire(self):
        print("$@#$#@$")


zoo_lst = [Dog("Brownie", 10), Cat("Zelda", 3), Skunk("Stinky"), Unicorn("Keith", 7), Dragon("Lizzy", 1450)]
zoo_lst.append(Dog("Doggo", 80))
zoo_lst.append(Cat("Kitty", 80))
zoo_lst.append(Skunk("Stinky Jr.", 80))
zoo_lst.append(Unicorn("Clair", 80))
zoo_lst.append(Dragon("McFly", 80))

for animal in zoo_lst:
    if animal.is_hungry():
         print(type(animal).__name__ + " " + animal.get_name())
         while animal.is_hungry():
            animal.feed()
    animal.talk()
    if isinstance(animal, Dog):
        animal.fetch_stick()
    if isinstance(animal, Cat):
        animal.chase_laser()
    if isinstance(animal, Skunk):
        animal.stink()
    if isinstance(animal, Unicorn):
        animal.sing()
    if isinstance(animal, Dragon):
        animal.breath_fire()

print(zoo_lst[0].zoo_name)

#unit 3
#3.1.3
def raise_stop_iteration():
    next(iter([]))

def raise_zero_division():
    1/0

def raise_assertion_error():
    assert False

def raise_import_error():
    import nonexistent_module

def raise_key_error():
    my_dict = {}
    my_dict['key']

def raise_syntax_error():
    eval('print("Hello, world!"')

def raise_indentation_error():
    def my_function():
        print("IndentationError")

def raise_type_error():
    "hello" + 5


# Call each function to raise the respective error
raise_stop_iteration()
raise_zero_division()
raise_assertion_error()
raise_import_error()
raise_key_error()
raise_syntax_error()
raise_indentation_error()
raise_type_error()

#3.2.5
def read_file(file_name):
    start = "__CONTENT_START__\n"
    end = "__CONTENT_END__"
    try:
        f = open(file_name, 'r')

    except IOError:
        value = "__NO_SUCH_FILE__\n"

    else:
        value = f.read()

    finally:
        return start + value + end

print(read_file("file_does_not_exist.txt")) #does not exist
print(read_file("C:\\Users\\User\PycharmProjects\\next.py\\name_length.txt")) #exists

#3.3.2
class UnderAge(Exception):
    def __init__(self, name, age):
        self._name = name
        self._age = age

    def __str__(self):
        return "your friend %s is underaged, wait %d years" % (self._name, 18 - self._age)

    def get_name(self):
        return self._name

    def get_age(self):
        return self._age


def send_invitation(name, age):
    try:
        if int(age) < 18:
            raise UnderAge(name, age)

    except UnderAge as e:
        print(e.__str__())

    else:
        print("You should send an invite to " + name)


send_invitation("Gil", 20)
send_invitation("Yoav", 17)

#3.4
import re
import string


class UsernameContainsIllegalCharacter(Exception):
    def __init__(self, username):
        self._username = username

    def __str__(self):
        illegal_chars = re.search(r'[^0-9a-zA-Z_]', self._username)
        char = illegal_chars.group()
        index = illegal_chars.start()
        return "The username contains an illegal character \"%c\" at index %d" % (char, index)

class UsernameTooShort (Exception):
    def __init__(self, username):
        self._username = username

    def __str__(self):
        return "username must be longer than 3"

class UsernameTooLong  (Exception):
    def __init__(self, username):
        self._username = username

    def __str__(self):
        return "username must be shorter than 16"

class PasswordMissingCharacter  (Exception):
    def __init__(self, password):
        self._password = password

    def __str__(self):
        return "password missing one of the characters "

class PasswordTooShort  (Exception):
    def __init__(self, password):
        self._password = password

    def __str__(self):
        return "password must be longer than 8"

class  PasswordTooLong (Exception):
    def __init__(self, password):
        self._password = password

    def __str__(self):
        return "password must be shorter than 40"


class Uppercase(PasswordMissingCharacter):
    def __init__(self, password):
        super().__init__(password)
        self._password = password

    def __str__(self):
        return super().__str__() + " (Uppercase)"

class Lowercase(PasswordMissingCharacter):
    def __init__(self, password):
        super().__init__(password)
        self._password = password

    def __str__(self):
        return super().__str__() + "(Lowercase)"

class Digit(PasswordMissingCharacter):
    def __init__(self, password):
        super().__init__(password)
        self._password = password

    def __str__(self):
        return super().__str__() + "(Digit)"

class Special(PasswordMissingCharacter):
    def __init__(self, password):
        super().__init__(password)
        self._password = password

    def __str__(self):
        return super().__str__() + "(Special)"

def check_input(username, password):
    if bool(re.search(r'[^0-9a-zA-Z_]', username)):
        raise UsernameContainsIllegalCharacter(username)
    elif len(username) < 3:
        raise UsernameTooShort(username)
    elif len(username) > 16:
        raise UsernameTooLong(username)
    elif not bool(re.search(r'[A-Z]', password)):
        raise Uppercase(password)
    elif not bool(re.search(r'[a-z]', password)):
        raise Lowercase(password)
    elif not bool(re.search(r'\d', password)):
        raise Digit(password)
    elif not any(char in string.punctuation for char in password):
        raise Special(password)
    elif len(password) < 8:
        raise UsernameTooShort(password)
    elif len(password) > 40:
        raise UsernameTooLong(password)

    else:
        print("OK")


check_input("1", "2")
check_input("0123456789ABCDEFG", "2")
check_input("A_a1.", "12345678")
check_input("A_1", "2")
check_input("A_1", "ThisIsAQuiteLongPasswordAndHonestlyUnnecessary")
check_input("A_1", "abcdefghijklmnop")
check_input("A_1", "ABCDEFGHIJLKMNOP")
check_input("A_1", "ABCDEFGhijklmnop")
check_input("A_1", "4BCD3F6h1jk1mn0p")
check_input("A_1", "4BCD3F6.1jk1mn0p")
check_input("A_1", "abcdefghijklmnop")
check_input("A_1", "ABCDEFGHIJLKMNOP")
check_input("A_1", "ABCDEFGhijklmnop")
check_input("A_1", "4BCD3F6h1jk1mn0p")

#unit4
#4.1.2
def translate(sentence):
    words = {'esta': 'is', 'la': 'the', 'en': 'in', 'gato': 'cat', 'casa': 'house', 'el': 'the'}
    generator = (words.get(spanish_word) for spanish_word in sentence.split() )
    new_str = ""
    for spanish_word in generator:
        new_str += spanish_word + " "
    return new_str

print(translate("el gato esta en la casa"))

#4.1.3
def is_prime(n):
    # Corner case
    if n <= 1:
        return False
    # Check from 2 to n-1
    for i in range(2, n):
        if n % i == 0:
            return False
    return True

def first_prime_over(n):
    prime_gen = (x for x in range(n + 1, 2 * n) if is_prime(x))
    return next(prime_gen)

print(first_prime_over(1000000))

#4.2.2
def parse_ranges(ranges_string):
    ranges = ranges_string.split(',')
    ranges_generator = (range(int(nums.split('-')[0]), int(nums.split('-')[1]) + 1) for nums in ranges)
    number_generator = (num for range_generated in ranges_generator for num in range_generated)
    return number_generator

print(list(parse_ranges("1-2,4-4,8-10")))
print(list(parse_ranges("0-0,4-8,20-21,43-45")))


#4.3.4
def get_fibo():
    x = 0
    y = 1
    print(x)
    print(y)
    while True:
        temp =  x + y
        x = y
        y = temp
        yield temp


fibo_gen = get_fibo()
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))

#4.4
def gen_secs():
    for num in range(0,60):
        yield num

def gen_minutes():
    for num in range(0,60):
        yield num

def gen_hours():
    for num in range(0,24):
        yield num

def gen_time():
    for h in gen_hours():
        for m in gen_minutes():
            for s in gen_secs():
                yield "%02d:%02d:%02d"%(h, m, s)


def gen_years(start=2019):
    yield start
    while True:
        start += 1
        yield start

def gen_months():
    for i in range(1, 13):
        yield i

def gen_days(month, leap_year=True):
    while True:
        i = 1
        if month % 2 == 1:
            num_of_days = 31
        elif month == 2:
            if leap_year:
                num_of_days = 29
            else:
                num_of_days = 28
        else:
            num_of_days = 30

        while i < num_of_days:
            yield i
            i += 1



def gen_date():
    for y in gen_years():
        leap_year = (y % 4 == 0 and y % 100 != 0) or y % 400 == 0
        for m in gen_months():
            for d in gen_days(m, leap_year):
                yield "%02d/%02d/%04d"%(d, m, y)

i = 0
for gd in gen_date():
    for gt in gen_time():
        i += 1
        if i % 1000000 == 0:
            print(gd + " " + gt)

#unit 5
#5.1.2
import winsound
freqs = {"la": 220,
         "si": 247,
         "do": 261,
         "re": 293,
         "mi": 329,
         "fa": 349,
         "sol": 392,
         }

notes = "sol,250-mi,250-mi,500-fa,250-re,250-re,500-do,250-re,250-mi,250-fa,250-sol,250-sol,250-sol,500"
notes_seperated = iter(notes.split('-'))
while True:
    try:
        curr = next(notes_seperated)
        frequency = freqs.get(curr.split(',')[0])
        duration = int(curr.split(',')[1])
        winsound.Beep(frequency, duration)
    except StopIteration:
        break

#5.2.2
numbers = iter(list(range(1, 101)))
for i in numbers:
    try:
        next(numbers)
        next(numbers)
        print(i)
    except StopIteration:
        break


#5.2.3
import itertools

money = [20, 20, 20, 10, 10, 10, 10, 10, 5, 5, 1, 1, 1, 1, 1]
counter = 0
list_of_possibilities = []
for i in range(1, len(money)):
    iterable_money = itertools.combinations(money, i)
    while True:
        try:
            list_of_bills = next(iterable_money)
            if sum(list_of_bills) == 100 and list_of_bills not in list_of_possibilities:
                list_of_possibilities.append(list_of_bills)
                print(list_of_bills)
                counter += 1
        except StopIteration:
            break

print("\nnumber of possibilities: " + str(counter))

#5.3.2
class Note():
    def __init__(self, note, freq):
        self._note = note
        self._freq = freq
        self._freq_index = -1

    def __iter__(self):
        return self

    def __next__(self):
        if self._freq_index > 3:
            raise Exception()
        self._freq_index += 1
        temp = self._freq
        self._freq *= 2
        return temp


class MusicNotes():
    def __init__(self):
        self._music_notes = []
        self._index = -1

    def add_music_note(self, note):
        self._music_notes.append(note)

    def __iter__(self):
        return self

    def __next__(self):
        if self._index == len(self._music_notes) - 1:
            self._index = -1
        self._index += 1
        try:
            return (next(self._music_notes[self._index]))
        except  Exception():
            raise Exception()



music_notes = MusicNotes()
music_notes.add_music_note(Note("La", 55))
music_notes.add_music_note(Note("Si", 61.74))
music_notes.add_music_note(Note("Do", 65.41))
music_notes.add_music_note(Note("Re", 73.42))
music_notes.add_music_note(Note("Mi", 82.41))
music_notes.add_music_note(Note("Fa", 87.31))
music_notes.add_music_note(Note("Sol", 98))

notes_iter = iter(music_notes)

for freq in notes_iter:
    print(freq)

#5.4
import functools


class IDIterator:
    def __init__(self, id):
        self._id = id

    def __iter__(self):
        return self

    def __next__(self):
        self._id += 1
        while not check_id_valid(self._id):
            if self._id > 999999999:
                raise StopIteration
            self._id += 1
        return self._id


def check_id_valid(id_number):
    id_number = [int(digit) * (2 if idx % 2 == 0 else 1) for idx, digit in enumerate(str(id_number), 1)]
    sum_of_digits = sum([1 + digits % 10 if digits > 9 else digits for digits in id_number])
    return sum_of_digits % 10 == 0

def id_generator(id_number):
    while True:
        if id_number > 999999999:
            raise StopIteration
        id_number += 1
        if check_id_valid(id_number):
            yield id_number


# print(check_id_valid(123456780))
# print(check_id_valid(123456782))

user_id = int(input("Enter ID: "))
user_option = input("Generator or Iterator? (gen/it)? ")

# Using iterator:
if user_option == "it":
    id_instance = IDIterator(user_id)
    id_iter = iter(id_instance)
    for i in range(10):
        print(next(id_iter))

# Using generator
elif user_option == "gen":
    gen_id = id_generator(user_id)
    for i in range(10):
        print(next(gen_id))

#unit 6
#6.1.3
import tkinter as tk
from PIL import ImageTk, Image

path = r"C:\Users\User\PycharmProjects\next.py\Dexter_Soldier_right_M.png"

def show_picture():
    image = Image.open(path)
    image = ImageTk.PhotoImage(image)
    label.configure(image=image)
    label.image = image  # Store a reference to the image to prevent it from being garbage collected

window = tk.Tk()
text = tk.Label(text="what is your favorite picture?")
text.pack()

# Create a label for the initial display
label = tk.Label(window)
label.pack()

# Create a button to show the picture
button = tk.Button(window, text="Show Picture", command=show_picture)
button.pack()

window.mainloop()

#6.1.4
import base64

# Encoded string
encoded_string = "CgkJICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAuLS0tW1tfX11dLS0tLS4KICAgICAgICAgICAgICA7LS0tLS0tLS0tLS0tLS58ICAgICAgIF9fX18KICAgICAgICAgICAgICB8ICAgICAgICAgICAgIHx8ICAgLi0tW1tfX11dLS0tLgogICAgICAgICAgICAgIHwgICAgICAgICAgICAgfHwgIDstLS0tLS0tLS0tLS58CiAgICAgICAgICAgICAgfCAgICAgICAgICAgICB8fCAgfCAgICAgICAgICAgfHwKICAgICAgICAgICAgICB8X19fX19fX19fX19fX3wvICB8ICAgICAgICAgICB8fAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHxfX19fX19fX19fX3wvCgo="

# Decode from Base64 to UTF-8 bytes
decoded_bytes = base64.b64decode(encoded_string)
decoded_message = decoded_bytes.decode('utf-8')

print("Decoded message:")
print(decoded_message)

#6.2.5
#the main code
from file1 import GreetingCard
from file2 import BirthdayCard

card = BirthdayCard(13, "gil", "you")
card2 = GreetingCard()

card.greeting_msg()
card2.greeting_msg()

#6.3.3
import pyttsx3
pyttsx3.speak("first time i'm using a package in next.py course")

#6.4
import matplotlib.pyplot as plt

# list of dots, in the following format: [x, y, x, y, x, y,...]
first = (
    146, 399, 163, 403, 170, 393, 169, 391, 166, 386, 170, 381, 170, 371, 170,
    355, 169, 346, 167, 335, 170, 329, 170, 320, 170, 310, 171, 301, 173, 290,
    178, 289, 182, 287, 188, 286, 190, 286, 192, 291, 194, 296, 195, 305, 194,
    307, 191, 312, 190, 316, 190, 321, 192, 331, 193, 338, 196, 341, 197, 346,
    199, 352, 198, 360, 197, 366, 197, 373, 196, 380, 197, 383, 196, 387, 192,
    389, 191, 392, 190, 396, 189, 400, 194, 401, 201, 402, 208, 403, 213, 402,
    216, 401, 219, 397, 219, 393, 216, 390, 215, 385, 215, 379, 213, 373, 213,
    365, 212, 360, 210, 353, 210, 347, 212, 338, 213, 329, 214, 319, 215, 311,
    215, 306, 216, 296, 218, 290, 221, 283, 225, 282, 233, 284, 238, 287, 243,
    290, 250, 291, 255, 294, 261, 293, 265, 291, 271, 291, 273, 289, 278, 287,
    279, 285, 281, 280, 284, 278, 284, 276, 287, 277, 289, 283, 291, 286, 294,
    291, 296, 295, 299, 300, 301, 304, 304, 320, 305, 327, 306, 332, 307, 341,
    306, 349, 303, 354, 301, 364, 301, 371, 297, 375, 292, 384, 291, 386, 302,
    393, 324, 391, 333, 387, 328, 375, 329, 367, 329, 353, 330, 341, 331, 328,
    336, 319, 338, 310, 341, 304, 341, 285, 341, 278, 343, 269, 344, 262, 346,
    259, 346, 251, 349, 259, 349, 264, 349, 273, 349, 280, 349, 288, 349, 295,
    349, 298, 354, 293, 356, 286, 354, 279, 352, 268, 352, 257, 351, 249, 350,
    234, 351, 211, 352, 197, 354, 185, 353, 171, 351, 154, 348, 147, 342, 137,
    339, 132, 330, 122, 327, 120, 314, 116, 304, 117, 293, 118, 284, 118, 281,
    122, 275, 128, 265, 129, 257, 131, 244, 133, 239, 134, 228, 136, 221, 137,
    214, 138, 209, 135, 201, 132, 192, 130, 184, 131, 175, 129, 170, 131, 159,
    134, 157, 134, 160, 130, 170, 125, 176, 114, 176, 102, 173, 103, 172, 108,
    171, 111, 163, 115, 156, 116, 149, 117, 142, 116, 136, 115, 129, 115, 124,
    115, 120, 115, 115, 117, 113, 120, 109, 122, 102, 122, 100, 121, 95, 121,
    89, 115, 87, 110, 82, 109, 84, 118, 89, 123, 93, 129, 100, 130, 108,
    132, 110, 133, 110, 136, 107, 138, 105, 140, 95, 138, 86, 141, 79, 149,
    77, 155, 81, 162, 90, 165, 97, 167, 99, 171, 109, 171, 107, 161, 111,
    156, 113, 170, 115, 185, 118, 208, 117, 223, 121, 239, 128, 251, 133, 259,
    136, 266, 139, 276, 143, 290, 148, 310, 151, 332, 155, 348, 156, 353, 153,
    366, 149, 379, 147, 394, 146, 399
)
second = (
    156, 141, 165, 135, 169, 131, 176, 130, 187, 134, 191, 140, 191, 146, 186,
    150, 179, 155, 175, 157, 168, 157, 163, 157, 159, 157, 158, 164, 159, 175,
    159, 181, 157, 191, 154, 197, 153, 205, 153, 210, 152, 212, 147, 215, 146,
    218, 143, 220, 132, 220, 125, 217, 119, 209, 116, 196, 115, 185, 114, 172,
    114, 167, 112, 161, 109, 165, 107, 170, 99, 171, 97, 167, 89, 164, 81,
    162, 77, 155, 81, 148, 87, 140, 96, 138, 105, 141, 110, 136, 111, 126,
    113, 129, 118, 117, 128, 114, 137, 115, 146, 114, 155, 115, 158, 121, 157,
    128, 156, 134, 157, 136, 156, 136
)

first_x = first[::2]
first_y = first[1::2]

second_x = second[::2]
second_y = second[1::2]



# set the title of a plot
plt.title("Connected Scatterplot points with lines")

# plot scatter plot with x and y data
plt.scatter(first_x, first_y)
plt.scatter(second_x, second_y)

# plot with x and y data
plt.plot(first_x, first_y)
plt.plot(second_x, second_y)

plt.show()